<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class Controller1Controller extends AbstractController
{
    /**
     * @Route("/controller1", name="controller1")
     */
    public function index()
    {
        return $this->render('controller1/index.html.twig', [
            'controller_name' => 'Controller1Controller',
        ]);
    }

    /**
     * @Route("/controller1/response", name="response1")
     */
    public function monAction1()
    {
        return new Response("Ce controller est nouveau et je suis juste une action à l'intérieur");
    }

    /**
     * @Route("/controller1/belgique")
     */
    public function belgique()
    {
        return new Response("Vive la Belgique");
    }

    // avec des paramètres dans url + tableau et boucle
    /**
     * @Route("/controller1/moyenne/{nb1}/{nb2}/{nb3}")
     */
    public function moyenne(Request $req)
    {
        $total = $req->get("nb1") + $req->get("nb2") + $req->get("nb3");
        
        return $this->render('controller1/moyenne.html.twig', [
            'nbs' => [$req->get("nb1"), $req->get("nb2"), $req->get("nb3")],
            'moyenne' => $total/3
        ]);

    }

    /**
     * @Route("/controller1/depassement/{chiffre1}/{chiffre2}")
     */
    public function depassement(Request $req)
    {
        return $this->render('controller1/depassement.html.twig', [
            'chiffre1' => (double)$req->get("chiffre1"),
            'chiffre2' => (double)$req->get("chiffre2"),
        ]);
    }

    /**
     * @Route("/controller1/redir/{param}")
     */
    public function redi(Request $req)
    {
        $site = $req->get("param");

        if (strlen($site) > 5) {
            return $this->redirect("https://www.google.com");
        }
        else {
            return $this->redirect("https://duckduckgo.com");
        }
    }
}
